#  Be sure to run `pod spec lint “base-spec”.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
Pod::Spec.new do |spec|
  spec.name         = "DzAVPlayerAdapter"
  spec.version      = "0.0.1-alpha2"
  spec.summary      = "Datazoom iOS/tvOS Native collector SDK"
  spec.description  = <<-DESC
          Datazoom Native iOS/tvOS collector SDK, used with AVPlayer, collects the events from a player and send to Datazoom pipeline
        DESC

  spec.homepage     = "https://www.datazoom.io/"
  spec.license      = { :type => 'Custom', :text => <<-LICENSE
                        Datazoom, Inc ("COMPANY") CONFIDENTIAL
 Copyright (c) 2017-2022 [Datazoom, Inc.], All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
 herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
 information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
 OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
 LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
                  LICENSE
                }
  spec.author             = { "Archit Dhupar" => "archit@datazoom.io" }
  spec.source       = { :http => "https://dz-ios-collectors-builds.s3.amazonaws.com/Podspec/DzAVPlayerAdapter/0.0.1-alpha2/DzAVPlayerAdapter-podspec.xcframework.zip" }

  spec.vendored_frameworks = 'DzAVPlayerAdapter.xcframework',
                             'DzBase.xcframework'

  spec.ios.deployment_target = '12.1'
  spec.swift_versions = '5.7.1'
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end
